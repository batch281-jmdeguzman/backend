console.log("hello World")


//javascript operators
//arithmitic operators

let x = 1397;
let y = 7831;

let sum = x+y;

console.log("result of addition operator: " + sum);

let difference = x-y;
console.log("result of difference operator: " + difference);

let product = x*y;
console.log("result of multiplication operator: " + product);

let qoutient = x/y;
console.log("result of division	operator: " + qoutient);

let remainder = y % x;
console.log("result of modulo operator: " + remainder);

//assignment operators (=) - it assigns the value of the right operand to a variable

let assignmentNumber = 8;

// addtion assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log('result of addition assignment operator ' + assignmentNumber);

// shorthand 
assignmentNumber +=2;
console.log("result of addition shorthand version assignment operator: " + assignmentNumber);
// subtruction/division/multiplication/ assignment operators (-=, *=, /=)

assignmentNumber -=2;
console.log("result of subtruction assigment operator: " + assignmentNumber);
assignmentNumber *=2;
console.log("result of multiplication assigment operator: " + assignmentNumber);
assignmentNumber /=2;
console.log("result of division assigment operator: " + assignmentNumber);


// Multiple operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of Mdas operation: " + mdas);

// using parentherses

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2-3)) * (4 / 5);
console.log("result of pemdas operation: " + pemdas);

// increament and decrement (++) (--)
// pre-increment 


let z=1;
let increment = ++z;
console.log('result of the pre-increment: ' + increment);
console.log('result of the pre increment: ' + increment);

// post increment
increment = z++;
console.log('result of the post-increment: ' + increment);
console.log('result of the post-increment: ' + z);

let decrement = --z;
console.log("Result of predecrement: " + decrement);
console.log("Result of predecrement: " + z);

decrement = z--;
console.log("Result of Post-decrement: " + decrement);
console.log("Result of Post-decrement: " + z);

// type coersion

let numA = '10';
let numB = 12;
let coercion = numA+numB;
console.log(coercion);
console.log(typeof coercion);

let numC = false + 1;
console.log(numC);

// comparison operators ()

let juan = 'juan';

// Equality Operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == juan)

// Inequality Operator (!=)

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != juan)

// Strict eQuality Operator (===) defends if string, boolean number, objects

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === juan)

// Strict Inequality Operator (!==)

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== juan)

// relational Operators (<>)

let a = 50;
let b =65;
// greater than (>)

let isGreaterThan = a > b;

// less than
let isLessThan = a < b;

// greater than or equal (>=)
let isGTorEqual = a >= b;

// less than or equal (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// logical Operators

let isLegalAge =true;
let isRegistered =false;

// Logical and Operator (&&)
// Return true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered ;
console.log ("Result of logical and operator: " + allRequirementsMet)

// logical or operator (||)
// returns true if one of the operands are true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("result of logial OR operator: " + someRequirementsMet); 

// logical not operator (!)
// return the opposite value

let someRequirementsNotMet = !isRegistered;
console.log("result of logical not operator:" +someRequirementsNotMet);

