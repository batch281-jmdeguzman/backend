//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo() {
  return await 

  //add fetch here.


  fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then((response) => response.json())
    .then((json) => json);
}
getSingleToDo().then((single) => console.log(single));

// Getting all to do list item

async function getAllToDo() {
  return await 

  //add fetch here.


  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => (titles = json.map((item) => item.title)));
}

getAllToDo().then((titles) => console.log(titles));

// [Section] Getting a specific to do list item

async function getSpecificToDo() {

  return await 

  //Add fetch here.

  fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      dateCompleted: "07/09/21",
      status: "Complete"
    })
  })
    .then((response) => response.json())
    .then((json) => json);
}
getSpecificToDo().then((specific) => console.log(specific));

// [Section] Creating a to do list item using POST method

async function createToDo() {
  return await 

  //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "Created To Do List Item",
      userId: 1,
      completed: false
    })
  })
    .then((response) => response.json())
    .then((json) => json);
}
createToDo().then((create) => console.log(create));

// [Section] Updating a to do list item using PUT method
async function updateToDo() {
  return await

   //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "Updated to Do List item",
      description: " To update my to do list with a different data structure",
      status: "Pending",
      dateCompleted: "Pending",
      userId: 1
    })
  })
    .then((response) => response.json())
    .then((json) => json);
}
updateToDo().then((update) => console.log(update));

// [Section] Deleting a to do list item
async function deleteToDo() {

  return await 


  //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "DELETE"
  });
}

deleteToDo();

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    getSingleToDo: typeof getSingleToDo !== "undefined" ? getSingleToDo : null,
    getAllToDo: typeof getAllToDo !== "undefined" ? getAllToDo : null,
    getSpecificToDo:
      typeof getSpecificToDo !== "undefined" ? getSpecificToDo : null,
    createToDo: typeof createToDo !== "undefined" ? createToDo : null,
    updateToDo: typeof updateToDo !== "undefined" ? updateToDo : null,
    deleteToDo: typeof deleteToDo !== "undefined" ? deleteToDo : null
  };
} catch (err) {}
