let http = require("http");

let port = 4000;

let app = http.createServer(function (request, response) {
  if (request.url == "/items" && request.method == "GET") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Data retrieved from the database using GET Method");
  }
  if (request.url == "/items" && request.method == "POST") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Data retrieved from the database POST Method");
  }
});

app.listen(port, () => console.log("Server is running at localhost:4000"));
