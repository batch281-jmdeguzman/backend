let http = require("http");

// Mock database
let directory = [
  {
    name: "Brandon",
    email: "brandon@mail.com"
  },
  {
    name: "Jobert",
    email: "jobert@mail.com"
  }
];

let port = 4000;

let app = http.createServer(function (request, response) {
  // router for returning all item upon receiving a GET request
  if (request.url == "/users" && request.method == "GET") {
    // sets status code 200 and response output to json data type
    response.writeHead(200, { "Content-type": "application/json" });
    response.write(JSON.stringify(directory));
    response.end();
  }
  //   ROute for creating a new data upon receiving a POST request
  if (request.url == "/users" && request.method == "POST") {
    // requestBody acts as a placeholder for the resources data be created later on
    let requestBody = "";

    // Stream is a sequence of data
    request.on("data", function (data) {
      // assigns the data retrieved from data stream to requestBody
      requestBody += data;
    });
    request.on("end", function () {
      console.log(typeof requestBody);

      // Converts the string requestBody to JSON
      requestBody = JSON.parse(requestBody);
      //   create a new obkect representing the new mock database
      let newUser = {
        name: requestBody.name,
        email: requestBody.email
      };
      // add the new user to the mock database
      directory.push(newUser);
      console.log(directory);

      response.writeHead(200, { "Content-Type": "application/JSON" });
      response.write(JSON.stringify(newUser));
      response.end();
    });
  }
});

app.listen(port, () => console.log("Server running at localhost:4000"));
