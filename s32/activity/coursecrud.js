//Add code here
let http = require("http");

// Mock database
let courses = [
  {
    courseName: "HTML",
    courseDescription: "Introduction to HTML"
  },
  {
    courseName: "CSS",
    courseDescription: "Introduction to CSS"
  }
];

let port = 4000;

let app = http.createServer(function (request, response) {
  // router for returning all item upon receiving a GET request
  if (request.url == "/" && request.method == "GET") {
    // sets status code 200 and response output to json data type
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Welcome to Booking System");
  }
  if (request.url == "/courses" && request.method == "GET") {
    response.writeHead(200, { "Content-type": "application/json" });
    response.write("Here's our Available Courses for this year");
    response.write(JSON.stringify(courses));
    response.end("");
  }
  if (request.url == "/addCourses" && request.method == "POST") {
    // requestBody acts as a placeholder for the resources data be created later on
    let requestBody = "";

    // Stream is a sequence of data
    request.on("data", function (data) {
      // assigns the data retrieved from data stream to requestBody
      requestBody += data;
    });
    request.on("end", function () {
      console.log(typeof requestBody);

      // Converts the string requestBody to JSON
      requestBody = JSON.parse(requestBody);
      //   create a new obkect representing the new mock database
      let newCourses = {
        courseName: requestBody.courseName,
        courseDescription: requestBody.courseDescription
      };
      // add the new user to the mock database
      courses.push(newCourses);
      console.log(courses);

      response.writeHead(200, { "Content-Type": "application/JSON" });
      response.write(JSON.stringify(newCourses));
      response.end();
    });
  }

  // Delete Method

  if (request.url == "/deleteCourses" && request.method == "DELETE") {
    // requestBody acts as a placeholder for the resources data be created later on
    let requestBody = "";

    // Stream is a sequence of data
    request.on("data", function (data) {
      // assigns the data retrieved from data stream to requestBody
      requestBody -= data;
    });
    request.on("end", function () {
      console.log(typeof requestBody);

      // Converts the string requestBody to JSON
      requestBody = JSON.parse(requestBody);
      //   create a new obkect representing the new mock database
      let deleteCourses = {
        courseName: requestBody.courseName,
        courseDescription: requestBody.courseDescription
      };
      // add the new user to the mock database
      courses.pop(deleteCourses);
      console.log(courses);

      response.writeHead(200, { "Content-Type": "application/JSON" });
      response.write(JSON.stringify(newCourses));
      response.end();
    });
  }
});

//Do not modify
//Make sure to save the server in variable called app
if (require.main === module) {
  app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
