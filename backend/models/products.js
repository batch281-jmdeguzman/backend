const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({

	name : {
		type: String,
		required: [true, "Product Name is required"]
	},
	brand : {
		type: String,
		required: [true, "Brand Name is required"]
	},
	description : {
		type: String
	},
	price : {
		type: Number,
		required: [true, "Product Price is required"]
	},
	productType : {
		type: String,
	},
	stock : {
		type: Number,
		required: [true, "Number of stock is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	onSale:{
		type: Boolean,
		default: false
	},
	image:{
		type: String,
	},
	createdOn: {
		type: Date,
		default: new Date()
	}


})

module.exports = mongoose.model("Product", productSchema);