const Order = require("../models/order");
const Product = require("../models/products");
const auth = require("../auth");

//addtocart features
module.exports.addToCart = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);

  if (!userData.isAdmin) {
    let userId1 = userData.id;

    let cartProduct = new Order({
      userId: userId1,
    });

    return cartProduct
      .save()
      .then((order) => {
        console.log(cartProduct);
        response.send(order._id);
      })
      .catch((error) => {
        console.log(error);
        response.send(`There's an error`);
      });
  } else {
    return response.send(`You are not a user`);
  }
};

module.exports.addToCartNext = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);

  if (!userData.isAdmin) {
    let orderId3 = request.params.orderId;

    let pushProduct = {
      //orderId : orderId3,
      productId: request.body.productId,
      quantity: request.body.quantity,
    };

    let productDetails = Product.findById(pushProduct.productId)
      .then((result) => {
        console.log(result);

        console.log(result.price);

        let addCartUpdate = Order.findById(orderId3).then((orderCart) => {
          orderCart.products.push({
            productId: pushProduct.productId,
            quantity: pushProduct.quantity,
            price: result.price,
            totalPrice: result.price * pushProduct.quantity,
          });

          return orderCart
            .save()
            .then((success) => {
              console.log(addCartUpdate);
              response.send(success);
            })
            .catch((error) => {
              console.log(error);
              response.send(`There's an error`);
            });
        });
      })
      .catch((err) => {
        response.send(`Error products`);
      });
  } else {
    return response.send(`You are not a user`);
  }
};

module.exports.checkout = (request, response) => {
  let token = request.headers.authorization;
  const userData = auth.decode(token);
  let dateNow = new Date();

  if (!userData.isAdmin) {
    let orderId1 = request.params.orderId;

    let order = Order.findById(orderId1).then((result) => {
      let totalAmount1 = 0;

      for (let i = 0; i < result.products.length; i++) {
        totalAmount1 += result.products[i].totalPrice;
      }

      console.log(totalAmount1);

      let data = {
        totalAmount: totalAmount1,
        purchaseDate: dateNow,
      };

      return Order.findByIdAndUpdate(orderId1, data, { new: true })
        .then((success) => {
          response.send(success);
        })
        .catch((err) => {
          response.send(`There's an error.`);
        });
    });
  } else {
    return response.send(`You are not a user`);
  }
};

