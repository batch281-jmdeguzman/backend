//Setup Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

//Allow all resources to access
app.use(cors());
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const orderRoutes = require("./routes/orderRoutes");

app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//Routes file

app.use("/orders", orderRoutes);

app.use("/products", productRoutes);
app.use("/users", userRoutes);

//Database connection
mongoose.set('strictQuery', false)
mongoose.connect("mongodb+srv://johnmillerdeguzman:JXXE8Vd3hGP5cBBK@wdc028-course-booking.vvgm1sd.mongodb.net/E-Commerce_API?retryWrites=true&w=majority", {

	useNewUrlParser: true,
	useUnifiedTopology: true
});
//Catch error or successfully access database
mongoose.connection.on("error", console.error.bind(console, "Connection error"));
mongoose.connection.once('open', () => console.log("Now connected to MongoDB"));

//Hosting service
app.listen(process.env.PORT || 4000, () =>{
	console.log (`API is now online on port ${process.env.PORT||4000}`)
});