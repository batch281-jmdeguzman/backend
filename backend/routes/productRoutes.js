const express = require("express");
const router = express.Router();
const auth = require("../auth")
const productControllers = require("../controllers/productControllers")
//routes for creating new Product
router.post("/createdNew", auth.verify, productControllers.addProduct)
//routes for retrieve all active products
router.get("/viewActive", productControllers.activeProduct)
//routes for retrieve all active products
router.get("/viewOnsaleProduct", productControllers.viewOnsaleProduct)


//routes for retrieve all added products
router.get("/list", auth.verify, productControllers.allProduct);
//routes for specific product
router.get("/:productId", productControllers.retrieveSpecificProduct);
//routes for updating a product
router.put("/update/:productId",auth.verify, productControllers.updateProduct);
//routes for archive product
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct)
//routes for unarchive product
router.patch("/unarchive/:productId", auth.verify, productControllers.unarchiveProduct)
//routes for setting onsale product
router.patch("/onsale/:productId", auth.verify, productControllers.onsaleProduct)
//routes for unset onSale product
 router.patch("/unsetonsale/:productId", auth.verify, productControllers.unsetonsaleProduct)
 
 


module.exports = router;