// check CamelCasing
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then((result) => {
		// The find method rreturns a record if a match is found
		if (result.length > 0) {
			return true;
			// No duplicate emails found
			// The user is not register
		} else {
			return "User does not Exist";
		}
	});
};

// user registration

module.exports.registerUser = (reqBody) => {
	// create a variable named "newUser" and insantiates a new 'User' object using the mongoose model
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if (error) {
			// user reg failed
			return false;
		} else {
			// user reg successful
			return "User registration success!";
		}
	});
};

// login user user authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result == null) {
			return "You're not registered! Please contact system administrator!";
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			);
			// if the passsword is correct
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) };
			} else {
				return false;
			}
		}
	});
};

// get specific user and display all details
module.exports.getUser = (reqParams) => {
	return User.findById(reqParams.userId).then((result) => {
		if (result == null) {
			return "There's no registered user for that ID";
		} else {
			return result;
		}
	});
};

// set admin
module.exports.setUser = (reqParams) => {
	let setUser = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqParams.userId, setUser).then(
		(user, error) => {
			if (error) {
				return false;
			} else {
				return "Setting admin succesfully";
			}
		}
	);
};

// set admin as user

module.exports.setAdmin = (reqParams) => {
	let setAdmin = {
		isAdmin: false
	};

	return User.findByIdAndUpdate(reqParams.userId, setAdmin).then(
		(user, error) => {
			if (error) {
				return false;
			} else {
				return "Setting user role succesfully";
			}
		}
	);
};

// checkout process

// module.exports.createOrder = async (reqUserId, data) => {
// 	const product = await Product.findById(data.productId);
// 	let totalAmount = data.quantity * product.price;
// 	let isUserUpdated = await User.findById(reqUserId).then((user) => {
// 		user.orderedProduct.push({
// 			products: {
// 				productId: data.productId,
// 				productName: data.productName,
// 				quantity: data.quantity
// 			},
// 			totalAmount,
// 			purchasedOn: new Date()
// 		});

// 		return user.save().then((user, error) => {
// 			if (error) {
// 				return false;
// 			} else {
// 				return true;
// 			}
// 		});
// 	});

// 	let isProductUpdated = await Product.findById(data.productId).then(
// 		(product) => {
// 			product.userOrders.push({ userId: data.userId });

// 			return product.save().then((product, error) => {
// 				if (error) {
// 					return false;
// 				} else {
// 					return true;
// 				}
// 			});
// 		}
// 	);

// 	if (isUserUpdated && isProductUpdated) {
// 		return "Succesfully checkout!";
// 		// User checkout failure
// 	} else {
// 		return false;
// 	}
// };

// Retrieve authenticated orders
// module.exports.userOrders = (data) => {

// 	return User.findById(data).then(result => {
// 		result = User.orderedProduct;
// 		return result;

// 	});

// }

module.exports.createOrder = async (data) => {
	const product = await Product.findById(data.productId);
	let totalAmount = data.quantity * product.price;
	let isUserUpdated = await User.findById(data.userId).then((user) => {
		user.orderedProduct.push({
			products: [
				{
					productId: data.productId,
					productName: data.productName,
					quantity: data.quantity
				}
			],
			totalAmount
		});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(
		(product) => {
			product.userOrders.push({ userId: data.userId });
			return product.save().then((product, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			});
		}
	);
	if (isUserUpdated && isProductUpdated) {
		result = "Checkout success!";
		return result;
	} else {
		return false;
	}
};

// Retrieve all active courses
module.exports.userOrders = (data) => {
	return User.findById(data.userId).then((result) => {
		result = result.orderedProduct;
		return result;
	});
};
