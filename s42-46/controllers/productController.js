const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});

	return newProduct.save().then((product, error) => {
		if (error) {
			return "You're not admin!";
		} else {
			return "Product added successfully";
		}
	});
};

// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result;
	});
};

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then((result) => {
		return result;
	});
};

// Retrieving a specific Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then((result) => {
		if (result == null) {
			return "No Product for that ID";
		} else {
			return result;
		}
	});
};

// Update a specific product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isFeatured: reqBody.isFeatured,
		isActive: reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(
		(product, error) => {
			// Product not updated
			if (error) {
				return "Product update failed!";

				// Course updated successfully
			} else {
				return "Product updated succesfully!";
			}
		}
	);
};

// archieve products

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(
		(product, error) => {
			// Products not archived
			if (error) {
				return false;

				// Products archived successfully
			} else {
				return "Products successfully archive!";
			}
		}
	);
};

// Activate a product
module.exports.activateProduct = (reqParams) => {
	let activateProduct = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, activateProduct).then(
		(product, error) => {
			if (error) {
				return `Product not activated.`;
			} else {
				return `Product activated successfully.`;
			}
		}
	);
};

// deActivate a product
module.exports.deactivateProduct = (reqParams) => {
	let deactivateProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, deactivateProduct).then(
		(product, error) => {
			if (error) {
				return `Product not deactivated.`;
			} else {
				return `Product Deactivated successfully.`;
			}
		}
	);
};

// stretch goal

// Retrieve all orders
module.exports.getAllOrders = () => {
	return Product.find({ isActive: true }).then((result) => {
		return result;
	});
};
