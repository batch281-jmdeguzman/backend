const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [
		{
			products: [
				{
					productId: {
						type: String
					},
					productName: {
						type: String
					},
					quantity: {
						type: Number,
						required: [true, "Quantity is required"]
					}
				}
			],

			totalAmount: {
				type: Number,
				default: 0
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);
