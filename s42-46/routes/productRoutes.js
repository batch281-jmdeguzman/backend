const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// add products
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin == true) {
		productController
			.addProduct(data)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController
		.getAllProducts()
		.then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving all active product

router.get("/", (req, res) => {
	productController
		.getAllActive()
		.then((resultFromController) => res.send(resultFromController));
});

// retrieve featured product
router.get("/featured", (req, res) => {
	productController
		.getAllFeatured()
		.then((resultFromController) => res.send(resultFromController));
});

// route for retrieving for specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController
		.getProduct(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

// route for update specfic product
router.put("/:productId", auth.verify, (req, res) => {
	productController
		.updateProduct(req.params, req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// check please
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin == true) {
		productController
			.archiveProduct(req.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Route for activating a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin) {
		productController
			.activateProduct(req.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		return `You must be an admin to activate a product.`;
	}
});

// Route for deactivating a product
router.put("/:productId/deactivate", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin) {
		productController
			.deactivateProduct(req.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		return `You must be an admin to deactivate a product.`;
	}
});

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin) {
		productController
			.updateProduct(req.params, data)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(`You must be an admin to update a product.`);
	}
});

module.exports = router;
