const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// checkEmail
router.post("/checkEmail", (req, res) => {
	userController
		.checkEmailExists(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// route for user registration

router.post("/register", (req, res) => {
	userController
		.registerUser(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// route for authentication
router.post("/login", (req, res) => {
	userController
		.loginUser(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// route for getting specific user details
router.get("/:userId", (req, res) => {
	console.log(req.params.userId);
	userController
		.getUser(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

// route for set user as admin
router.put("/:userId/setAdmin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin) {
		userController
			.setUser(req.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		return false;
	}
});

// route for set admin as user
router.put("/:userId/setUser", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if (data.isAdmin) {
		userController
			.setAdmin(req.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		return false;
	}
});

// checkout
// router.post("/:userId/checkout", auth.verify, (req, res) => {
// 	let data = {
// 		userId: req.body.userId,
// 		productId: req.body.productId,
// 		productName: req.body.productName,
// 		quantity: req.body.quantity
// 	};

// 	userController
// 		.checkout(req.params.userId, data)
// 		.then((resultFromController) => res.send(resultFromController));
// });

//Retrieve authenticated user orders
// router.get("/myOrders", auth.verify, (req,res) =>{
// 	const data = auth.decode(req.headers.authorization)
// 	userController.userOrders({}).then(resultFromController => res.send(resultFromController))
// })

router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: req.body.userId,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity
	};
	userController
		.createOrder(data)
		.then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving all the active courses
router.get("/myOrders", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);

	userController
		.userOrders({ userId: data.id })
		.then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
