const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// edit this
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// mongoDB Connection Start
mongoose.connect(
	"mongodb+srv://johnmillerdeguzman:JXXE8Vd3hGP5cBBK@wdc028-course-booking.vvgm1sd.mongodb.net/E-Commerce_API?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () =>
	console.log("Now connected to MongoDB atlas")
);
mongoose.connection.on(
	"error",
	console.error.bind(console, "MongoDb Connection error")
);

// End of connection in mongoDB

app.listen(process.env.PORT || 3000);
console.log(`API is now online on port ${process.env.PORT || 3000}`);
