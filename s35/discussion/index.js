const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// mongoDB connection
// connecting to mongodb atlas
mongoose.connect(
  "mongodb+srv://johnmillerdeguzman:JXXE8Vd3hGP5cBBK@wdc028-course-booking.vvgm1sd.mongodb.net/b281_to-do?retryWrites=true&w=majority",
  {
    // gives warning if there where problems in connections deprecation warning
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

// allows the appto read the json data
app.use(express.json());

// allow the app to read data from form

app.use(express.urlencoded({ extended: true }));

// set notification for connection success or failure
// mongoose handles if successful or it has error

let db = mongoose.connection;

// if a connection error occured, output in the console

db.on("error", console.error.bind(console, "connection error"));

// if connection is successfull, output in the console

db.once("open", () => console.log("We're connected to the cloud database"));

// MOngoose schema
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending"
  }
});

// Models [Task Models]

const Task = mongoose.model("Task", taskSchema);

// creatiom of todo list routes

// creating a new task
app.post("/tasks", (req, res) => {
  // if a document was found and document's name matches the information from the client
  Task.findOne({ name: req.body.name }).then((result, err) => {
    if (result !== null && result.name === req.body.name) {
      // return a message to the client/ postman
      return res.send("Duplicate task found");
    }
    // if no document was found
    else {
      // create a new task and save it to the database
      let newTask = new Task({
        name: req.body.name
      });
      newTask.save().then((saveTask, saveErr) => {
        // if there are error in  saving
        if (saveErr) {
          return console.error(saveErr);
        }
        // No error found while creating the document
        else {
          return res.status(201).send("new task created");
        }
      });
    }
  });
});

// Get all the tasks
app.get("/tasks", (req, res) => {
  Task.find({}).then((result, err) => {
    // If an error occured
    if (err) {
      // Will print any errors found in the console
      return console.log(err);
    }
    // If no errors are foud
    else {
      return res.status(200).json({
        data: result
      });
    }
  });
});

// activity part

const userSchema = new mongoose.Schema({
  username: String,
  password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  // if a document was found and document's name matches the information from the client
  User.findOne({ username: req.body.username }).then((result, err) => {
    if (result !== null && result.usernamename === req.body.username) {
      // return a message to the client/ postman
      return res.send("Duplicate Username found");
    }
    // if no document was found
    else {
      // create a new user and save it to the database
      let newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      newUser.save().then((saveUser, saveErr) => {
        // if there are error in  saving
        if (saveErr) {
          return console.error(saveErr);
        }
        // No error found while creating the document
        else {
          return res.status(201).send("Registered New User");
        }
      });
    }
  });
});

// Get all the users
app.get("/signup", (req, res) => {
  User.find({}).then((result, err) => {
    // If an error occured
    if (err) {
      // Will print any errors found in the console
      return console.log(err);
    }
    // If no errors are found
    else {
      return res.status(200).json({
        data: result
      });
    }
  });
});

// Listen to the port

app.listen(port, () => console.log(`Server Running at port ${port}`));
