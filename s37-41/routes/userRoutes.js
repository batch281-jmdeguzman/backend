const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the email already exists in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// route for user registration

router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// route for authentication
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// activity
// route for authentication
// router.post("/details", (req, res) => {
//   userController
//     .getProfile(req.body)
//     .then((resultFromController) => res.send(resultFromController));
// });

// the auth.verify acts as a middleware to wnsure that the user is logged in before the can enroll to a course
router.get("/details", auth.verify, (req, res) => {
  // user decode mehtod defined in the auth.js file to retrive user information from the token passing the "token" from request header
  const userData = auth.decode(req.headers.authorization);

  // Provides the user's ID for the getProfile controller method
  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

// Route to enrol user to a course
// this is acceptable also
// let data = {
//   userId: auth.decode(req.headers.authorization).id,
//   isAdmin:auth.decode(req.headers.authorization).isadmin,
//   courseId: req.body.courseId
// };

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {

  let data = {
    userData : auth.decode(req.headers.authorization),
    courseId : req.body.courseId
  }

  userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
