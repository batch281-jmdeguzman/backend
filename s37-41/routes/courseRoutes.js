const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// recopied desk
// activity
// router.post("/", auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);
//   if (userData.isAdmin == true) {
//     courseController
//       .addCourse(req.body)
//       .then((resultFromController) => res.send(resultFromController));
//   } else {
//     return res.status(401).send("You're Not an admin to add course");
//   }
// });

router.post("/", auth.verify, (req, res) => {
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  if (data.isAdmin == true) {
    courseController
      .addCourse(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
  courseController
    .getAllCourses()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving all active courses

router.get("/", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

// route for retrieving for specific course
// creating a course using "/:idparamname" create a dynamic route, meaning the url changes depending on the information provided

router.get("/:courseId", (req, res) => {
  console.log(req.params.courseId);
  // since the course id will be sent via url, we canoot retrieve it from the request bpdy
  // we can however retrive .....
  courseController
    .getCourse(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// // Route for updating a course
// // Route for archiving courses
// router.patch("/:courseId", auth.verify, (req, res) => {
//   const data = {
//     course: req.body,
//     isAdmin: auth.decode(req.headers.authorization).isAdmin
//   };
//   if (data.isAdmin == true) {
//     courseController
//       .archiveCourse(req.params, data)
//       .then((resultFromController) => res.send(resultFromController));
//   } else {
//     res.send(false);
//   }
// });

// S40 Activity
// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  if (data.isAdmin == true) {
    courseController
      .archiveCourse(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

module.exports = router;
