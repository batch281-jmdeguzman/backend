const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

// allows all resources to access our backend api
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Define the "/users" string to be included for all users rputes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// mongoDB Connection Start
mongoose.connect(
  "mongodb+srv://johnmillerdeguzman:JXXE8Vd3hGP5cBBK@wdc028-course-booking.vvgm1sd.mongodb.net/s37-41API?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

mongoose.connection.once("open", () =>
  console.log("Now connected to MongoDB atlas")
);
mongoose.connection.on(
  "error",
  console.error.bind(console, "MongoDb Connection error")
);

// End of connection in mongoDB

app.listen(process.env.PORT || 4000);
console.log(`API is now online on port ${process.env.PORT || 4000}`);
