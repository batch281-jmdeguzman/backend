const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    // The find method rreturns a record if a match is found
    if (result.length > 0) {
      return true;
      // No duplicate emails found
      // The user is not register
    } else {
      return false;
    }
  });
};

// user registration

module.exports.registerUser = (reqBody) => {
  // create a variable named "newUser" and insantiates a new 'User' object using the mongoose model
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10)
  });

  return newUser.save().then((user, error) => {
    if (error) {
      // user reg failed
      return false;
    } else {
      // user reg successful
      return true;
    }
  });
};

// login user user authentication

module.exports.loginUser = (reqBody) => {
  // the findone method returns the firs record in the collection that matches the search criteria
  // we use findone method instead of find method wich return all recird that match criteeria
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      // Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
      // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      // if the passsword is correct
      if (isPasswordCorrect) {
        // generate an acess token
        // uses the CreateAccessToken mehod defind in the auth.js file
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

// activity

// module.exports.getProfile = (reqBody) => {
//   return User.findOne({ _id: reqBody.id }).then((result) => {
//     if (result == null) {
//       return false;
//     } else {
//       result.password = "";
//       return result;
//     }
//   });
// };

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    // Changes the value of the user's password to an empty string when returned to the frontend
    // Not doing so will expose the user's password which will also not be needed in other parts of our application
    // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
    result.password = "";

    // Returns the user information with the password as an empty string
    return result;
  });
};

// Enroll user to a class
// async await will be used in enrolling tje user because we will need to update 2 docuemnt separeate documents when enrollong a user
module.exports.enroll = async (data) => {
  //  using the "await " keyword will allow the enroll method to complete updating the user before returning a response back
  let isUserUpdated = await User.findById(data.userId).then((user) => {
    // adds the courseId in the users enrollment array
    user.enrollments.push({ courseId: data.courseId });
    return user.save().then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });

  let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
    // adds the userId in the course's enrollees array
    course.enrollees.push({ userId: data.userId });
    return course.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });

  if (isUserUpdated && isCourseUpdated) {
    return true;
    // user enrollment failure
  } else {
    return false;
  }
};
