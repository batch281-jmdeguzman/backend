console.log("Hello, World");

let firstName = "First Name:";
let fName = "John Miller";
console.log(firstName + " " + fName);
let lastName = "Last Name:";
let lName = "De Guzman";
console.log(lastName + " " + lName);
let age = "Age:";
let myAge = "27";
console.log(age + " " + myAge);
let hobby = "Hobbies:";
let myHobby = ["Playing", "Eating", "Traveling", "Parties"];
console.log(hobby);
console.log(myHobby);
let workAddress = {
  HouseNumber: 32,
  street: "Loyola",
  city: "Lincolin",
  state: "Nebraska",
};
console.log("Work Address:");
console.log(workAddress);

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ");
console.log(friends);

let profile = {
  username: "captain_america",
  fullName: "Steve Rogers",
  age: 40,
  isActive: false,
};
//todo

console.log("My Full Profile: ");
console.log(profile);

// bawal double entry ng container or string?

let fullName1 = "Bucky Barnes";
console.log("My bestfriend is: " + fullName1);

const lastLocation = "Arctic Ocean";
//	lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);
