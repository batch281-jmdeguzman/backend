// Comments 

// Comments are part of the code that ignored by the language
// Comment are ment to describe the written code

/* Two types of comment 
1single line 
2multi line
*/


console.log("Hello, World!");

// variables
/*
	-it is used to contain data 

	*/
// declaring variables - tells our devices that a variable name is created and is ready to store data
/* syntax:
		let/const variableName; 
	let - keyword usually used in declaring a variable for specific code block
	const - keyword usally used in declaring a constant variable
	var - can use in any pero di na gagamitin to
		*/
let myVariable;

console.log(myVariable);
// console.log(hello);

// initializing variables -  the instance when a variable is given it's initial/starting value.
/*
	syntax: 
		let/const variables = value ;
*/

let productName = 'desktop computer'
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
// reassigning variable values
// syntax: variableName = newValue


productName = 'Laptop';
console.log(productName);
// interest=4.

// declares a variable first
let supplier;

// initialization of Value
supplier = "John Smith Tradings";
console.log(supplier)

//Multiple variable declaration

let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand); 

// data types

// String - handles a series of characters that create a word or phrase, o a sentence.

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating string + is used 
let fullAdress = province + ', ' + country;
console.log(fullAdress);

let greeting = 'I live in the ' + country;
console.log (greeting);
// Escape character (\) in strings is combanation with other characters can produce a different effect

let mailAddress = 'Metro Manila \n\nPhilippines';
console.log(mailAddress);

let message ="John's employee went home early";
console.log(message);

// Numbers
// Integears / Whole Numbers
let headcount=32;
console.log(headcount);

// decimal number or fraction

let grade =98.7;
console.log(grade);
// exponential notation
let planetDistance=2e10;
console.log(planetDistance);

// combining text/numbers and strings

console.log("John's grade last quarter is" + ' '+ grade)

// boolean - used to store values relating to the state of certain things

let isMarried = true;
let inGoodConduct = false;
console.log("isMarried:" + isMarried);
console.log("inGoodConduct:" + inGoodConduct);

// Array - are special kind of data type that's use to store multiple values;

//syntax : let/const arrayName=[elementA, elementB, etc];

let grades = [98.7, 92.1, 94.6, 90.2];
console.log(grades);

// Objects - another special kind of data type that's used to mimic real world objects/items

/*
	Syntax:
		let/const objectName= {
			propertyA:value,
			propertyB:value, 
		}
*/
	
let person={
	fullName : 'Juan Dela Cruz',
	age:35,
	isMarried:false,
	contact:["091235487895", "32135468131"],
	address:{
		houseNumber:'345',
		city:'Manila'
	}
};

console.log(person);

let	myGrade ={
	firstGrading:98.7,
	secondGrading:92.1,
	thirdGrading:90.2,
	fourthGrading:94.6
};
console.log(myGrade);


// type of operator - used to determining the type of data or the value of the variable.

console.log(typeof myGrade);
console.log(typeof grades);

//null - used to intentionally express the absence of a value in a variable declaration/initialization.

let spouse =null;
let myNumber = 0;
let myString='';

//undefined

