// console.log("hello World");

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer','Asus','Lenovo','Neo','Redfox','Gateway','toshiba','fuhitsu'];

let mixedArr = [12, 'Asus', null, undefined]; //this is not recommended

let tasks = [
    'drink',
    'eat javascript',
    'inhale css',
    'bake sass'
];

// creating an array with the valuues from variables

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Mumbai";

let cities = [ city1, city2, city3];
console.log(tasks);
console.log(cities);

// array lenth property
console.log("Array length");
console.log(tasks.length);
console.log(cities.length);

let fullName="Jhia Naomi Reign B. De Guzman";
console.log(fullName.length + fullName);

tasks.length = tasks.length - 1;
console.log(tasks.length);
console.log(tasks);

cities.length --;
console.log(cities);

fullName.length = fullName.length - 1;
console.log(fullName.length);

// adding a number to lenthen the size of the array
let theBeatles = ["john","paul","ringo","George"];
theBeatles.length++;
console.log(theBeatles)

// accessing element of an array
// when accessing index doesnt exit it will display undefined *_*
console.log(grades[0]);
console.log(grades[20]);
console.log(computerBrands[3]);

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];

//access the second item in the array
//access the fourth item in the array
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log("Accessing arrays using variables");
console.log(currentLaker);

// reassigning values in array
console.log("Array before reassigning");
console.log(lakersLegends);
lakersLegends[2] = "Paul Gasol"
console.log("Array after reassigning");
console.log(lakersLegends);

// Accessing the last element of an array
// .length starst with 1 unline counting in index
let bullsLegends = ['jordan','pippen','rodman','rose','Kukoc'];

let lastElementIndex = bullsLegends.length -1;

console.log(bullsLegends[lastElementIndex]);

// Directly access the expression

console.log(bullsLegends[bullsLegends.length-1]);

// adding items into the array

console.log("adding items into the array");
let newArray = [];
console.log(newArray[0]);

newArray[0]="cloud Stripe";
console.log(newArray);

let newVar = prompt("enter a name: ");
newArray[1] = newVar;
console.log(newArray);

//looping through array
// let computerBrands = ['Acer','Asus','Lenovo','Neo','Redfox','Gateway','toshiba','fuhitsu'];

for(let index = 0;index < computerBrands.length; index++){
    console.log(computerBrands[index]);
}

// Checks the element if it's divisible by 5

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++){
    if (numArr[index] % 5 === 0){
        console.log(numArr[index] + " is divisible by 5");
    }
    else{
        console.log(numArr[index] + " is not divisible by 5");
    }
}

// Multidimensional Array
// 
/*
// 2 x 2 dimentional array
/*
    let twoDim = [[element1,elemen2],[element3, element4]]
                    0           1         0          1
                            0                   1

    twoDim[0][0];
    twoDim[1][0]
*/
let twoDim = [['Kenzo','Alonzo','kobe'],['Bella','Jhia','bryan']];
console.log(twoDim[0][1]);
console.log(twoDim[1][0]);
console.log(twoDim[1][1]);

let twoDimLen = twoDim.length;
console.log(twoDimLen);
console.log(twoDim[0][1]+twoDim[1][0])


