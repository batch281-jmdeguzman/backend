console.log("hello world")

// JSON Object 
/*
    -JSON stands for javascript object notation 
        SYNTax:
        {
            "propertyA": "valueA",
            "propertyB": "valueB"       
        }
*/

/*{
    "city":"Quezon City",
    "province": "Metro Manila",
    "country":"Philippines"
}*/

// JSON Arrays

/*"cities": [
    {"city": "Quezon City", "province":"Metro Manila", "country":"Philippines"},
    {"city": "Manilal City", "province":"Metro Manila", "country":"Philippines"},
    {"city": "Makati City", "province":"Metro Manila", "country":"Philippines"}
]*/

// Mini Activity - Create a JSON Array that wil hold three breeds of dogs with properties: name ages breed
/*"breed": [
    {"name": "Falco", "age":"10", "breed":"Chitzu"}
    {"name": "Ash", "age":"8", "breed":"poodle"}
    {"name": "Sachi", "age":"12", "breed":"dalmatian"}
]*/

// JSON Methods
// Convert data into stingified JSON

let batchesArr = [{batchName: 'Batch x'}, {batchName:'Batch Y'}];

// the stringify method is used to convert JAVASCRIPT object into a string

console.log('Result from stringify method: ');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
    name:"john",
    age:"31",
    address:{
        city:"Manila",
        country:"Philippines"
    }
});
console.log(data);

// using stringyfy method with variables
// user details 

/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
    city : prompt('Which City do you live in?'),
    country : prompt('Whih country does your city address belong to?')
};

// need to be enclose to "" para malaman na json siya

let otherData = JSON.stringify ({
    firstName: firstName,
    lastName:lastName,
    age:age,
    address:address
})

console.log(otherData);
*/
// mini activity - create a json data that will accept user car details with variables brand type year


/*let brandName = prompt('What is your Car Brand?');
let type = prompt('What is the type of your car?');
let year = prompt('What year that your car launch ?');

let userCar = JSON.stringify ({
    brandName: brandName,
    type:type,
    year:year
})

console.log(userCar);*/

// converting stringified JSON onto Javascript Objects
let batchesJSON = `[{ " BatchName":"BatchX"},{" BatchName":"BatchY"}]`;

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));