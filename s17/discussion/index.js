console.log("hello World");


// /funtion

/*
	syntax:
	function functionName(){
	code block (statement)
	};

*/

function printName(){
	console.log("My name Is john Miller");
}
printName();

// funtion declaration vs function expression
// function declaration 
function declaredFunction(){
	console.log("Hello World from declaredFunction()");
}
declaredFunction();

// function expression anonymous function pag walang pangalan yung functon

let variableFunction = function(){
	console.log("hello again");
};

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

const constantFunc = function(){
	console.log	("Initalize with const!")

};
constantFunc();



/*constantFunc = function(){
	console	.log("cannot be rea -assigned")
}

constantFunc();*/

// function scoping

/*
		scope is the accessibiltiu (visibility of variable )

		JS Variable has 3 types of scope:
		1.local/block scope {}
		2. global scope
		3.function scope
*/

// local block scope

{
	let localVar ="Alonzo Mateo";
	console.log(localVar);
}

let globalVar = "aizaac ellis";
console.log(globalVar);

function showNames(){
	// function scope varialble
	var functionVar = "john doe";
	const functionConst = "jane";
	let functionLet = "jane";
	console.log (functionVar);
	console.log (functionConst);
	console.log (functionLet);
}

showNames();
	// console.log (functionVar);
	// console.log (functionConst);
	// console.log (functionLet);

// nested functions

function myNewFunction(){
	let name = "jane";

	function nestedFunction (){
		let nestedName = "john";
		console.log (name);
	}
	nestedFunction();
}

// nestedFunction();
myNewFunction();

// funciton and global variables
// global scope variable

let globalName = "joy";
function myNewFunction2(){
	let nameInside = "kenzo";

	console.log(globalName);
	console.log(nameInside)

}

myNewFunction2();

console.log(globalName);

// using alert

alert("hello World"); //this will run immediately when the page loads

function showSampleAlert(){
	alert("hello user");
}
showSampleAlert();

// using prompt()

let samplePromt = prompt("Enter your Name:");

console.log("Hello" + samplePromt);

function printWelcomeMessage(){
	let firstName = prompt("enter your first name");
	let lastName = prompt("enter your last name");

	console.log("hello" + firstName +" "+ lastName + "!");
	console.log("welcome to my page");

}

printWelcomeMessage();

/*function naming convention 
-function names should be definitive of the task it will perfrom
- avoid generic names to avoid confusion within the code
-avoid pointless and inapproriate function names
- name your function using or follwing camel casing.
- do not use javascript reserve keywords*/

