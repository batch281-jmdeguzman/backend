// console.log("hello World");

// While loop
// it evalauates a condition, if returned true, it will execute statement as long as the condtion is satified. if at first the condition is not satisfied, no statement will executed

/*
 syntax:
    while(expression/condition){
    statement/s;
    }
  
  
*/
/*
let count = 5;
while(count !== 0){
    console.log("While: " + count);
    count++;
}*/

console.log("While Loop");

let count = 5;
while(count !== 0){
    console.log("While: " + count);
    count--;
}


/*
    count:5, 4, 3, 2, 1
        console: 
        While: 5
        While: 4
        While: 3
        While: 2
        While: 1


*/


let count1 = 1;
while(count1 <= 5){
    console.log("While: " + count1);
    count1++;
}



/*
    count:1, 2, 3, 4, 5
        console: 
        While: 1
        While: 2
        While: 3
        While: 4
        While: 5


*/

// do while loop

/*
 - it iterates statements withins a number of times based on a condition.
 however, if the condition was not satisfied at first, one statement will be executed

    syntax:
    do{
        statement/s;
    }while(expression/condition);

*/

console.log("Do While")

let number = +(prompt("Give me a number"));

do{
    console.log("Do While: "+number);
    number += 1;
    // number = + 1
}while(number<10);



let even = 2;

do{
    console.log(even);
    even+=2;
}while(even<=10);

// for loop
/*
    - a looping construct that is more flexible than other loops. it consist of three parts:
    -inialization
    -condition / expression
    -final expression/ step expression

    syntax:
        for(initialization; expression/condition; finalExpression){
            statement/s;
        }

*/

console.log("For Loop");

for(let count = 0; count <= 20; count++){
    console.log(count);
}

/*
    count=0, 1, 2. . .
    console:
    0
    1
    2
    3 . . 

*/
console.log("looping through array");
let myString = "alex ";

console.log(myString.length);

// accessing element of a sting

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);



for(let x= 0; x< myString.length; x++){
    console.log(myString[x])
}

let myName = "AlExIs";

console.log("Looping through vowels and consonant")
for(let i = 0; i < myName.length; i++){
    if(
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u" 
        ){
        console.log(3);
    }
    else{
        console.log(myName[i]);
    }
}

// Continue and break statements
for(let count=0; count<=20; count++){
    if(count%2===0){
        // Tells the code to continue to tje next iteration of the loop
        continue;
        
    }
    console.log("Continue and break: " +count )
    // if the current value of count is greater than 10 stops the loop
    if(count>10){
        //tells the code to terminate/stop the loop even if the condition of the loop defines that it should execute
       break;     
    }
}

// 
let name ="alexandro";

for(let i=0; i<name.length;i++){
    console.log(name[i]);

    //if the charaacter is equal to 'a' continue to the next iteration
    if(name[i].toLowerCase() === "a"){
        console.log("Continue to the next iteration");
        continue;
    }

    //if the current letter is equal to'd', stop the loop
    if(name[i]=="d"){
        break;
    }
}