// console.log("Hello World");

let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

// Creates a loop that will iterate through the whole string
for (let i=0; i < string.length; i++) {

	// Check what is the starting value of the loop
	// console.log(string[i]);
	
	// If the current letter being evaluated is a vowel
	if (
		string[i].toLowerCase() == 'a' ||
		string[i].toLowerCase() == 'e' ||
		string[i].toLowerCase() == 'i' ||
		string[i].toLowerCase() == 'o' ||
		string[i].toLowerCase() == 'u'
	) {

		// Continue the loop to the next letter/character in the sequence
		continue;

	// If the current letter being evaluated is not a vowel
	} else {

		// Add the letter to a different variable
		filteredString += string[i];

	}

}

// After the loop is complete, print the filtered string without the vowels
console.log(filteredString);


//Do not modify
//For exporting to test.js
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}
