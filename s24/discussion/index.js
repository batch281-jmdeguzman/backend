// exponent Operator (**)

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template literals es6 updates

let name = "John";

// pre-template literal string
let message = "Hello" + name + " ! Welcome to Programming!";
console.log("Message without template literals: " + message);

// String using template literals (``)
message = `Hello ${name}! Welcome to Programming using template literals`;
console.log(`message with template Literals: ${message}`);

// Multiple line using template literals
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}
`;
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;
console.log(
  `The interest on your saving account is : ${principal * interestRate}`
);

// Array destructuring
/*
	-Allows us to unpack element in arrays into distinct variables
	- Allows us to name array element with variables instead of using index number
	Syntax:
	let/const [variableName, variableName, variableName, ]=array;
*/

const fullName = ["Juan", "Castro", "Dela Cruz"];
// pre Array destructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! Its nice to meet you`
);

// array destructuring
const [firstName, middleName, lastName] = fullName;
console.log(
  `Hello ${firstName} ${middleName} ${lastName}! Its nice to meet you`
);

// Object Destructuring

/*
	-allows us to unpack properties of object into distinct variables
	Syntax:
		let/const {propertyName, propertyName, propertyName,} = object;
*/

const person = {
  givenName: "Jane",
  maidenName: "Castro",
  familyName: "Dela Cruz",
};
// pre Object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! it's Good to see you again!`
);

// object destructuring
const { givenName, maidenName, familyName } = person;
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! it's Good to see you again!`
);

// arrow fucntions

/*
	-Compact alternative syntax to traditional functions
	syntax:	
		const/let variableName = () = >{
			console.log()
		}
*/

const hello = () => {
  console.log("Hello, World!");
};

// pre-arrow function and template literals

/*
	function printFullName (firstName, middleInitial, lastName){
		console.log(firstName + '' +'middleInitial + '. ')+lastName);

	}
	printFullName ("John","D","Smith");
*/

// IMplementation of arrow function

const printFullName = (firstName, middleInitial, lastName) => {
  console.log(`${firstName} ${middleInitial} ${lastName}`);
};

printFullName("John", "D", "Smith");

// arrow functions with loops
// pre-arrow function
const students = ["john", "Jane", "Joy"];

console.log("Using traditional function");
students.forEach(function (student) {
  console.log(`${student} is a student.`);
});

// arrow functions in loops
students.forEach((student) => {
  console.log(`${student} is a student.`);
});

// Implicit return statement
// Pre-arrow function

/*
	const add = (x,y)=>{
		return + y;
	}
	let total = add(1,2);
	console.log(total);
*/
// implicit return
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);

// Default function argument value
const greet = (name = "User") => {
  return `Goodmorning, ${name}!`;
};
console.log(greet());
console.log(greet("Ray"));

// Class based object blueprints

//creating a class parang container siya na lalagyan ng mga values para sa lahat
// to summarize the codebase

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

// Instatiating and object

const myCar = new Car();

console.log(myCar);

// Values of properties assigned after instatiation of an object
myCar.brand = "Ford";
myCar.name = "Ford Ranger";
myCar.year = 2021;

console.log(myCar);

// instatiate a new object from the car class with iniatialized values

const myNewCar = new Car("Toyota", "Vios", "2023");
console.log(myNewCar);

console.log("Hello World!");
