// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// importing all data from taskRoute
// server <= route <= controller <= model
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Database connection
// Connecting to MongoDB Atlas

mongoose.connect(
  "mongodb+srv://johnmillerdeguzman:JXXE8Vd3hGP5cBBK@wdc028-course-booking.vvgm1sd.mongodb.net/",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

// displays a text that we are succesfully connected to db

let db = mongoose.connection;

// if a connection error occured, output in the console

db.on("error", console.error.bind(console, "connection error"));

// if connection is successfull, output in the console

db.once("open", () => console.log("We're connected to the cloud database"));

// add the task route
//
app.use("/tasks", taskRoute);

// Server listenin

if (require.main === module) {
  app.listen(port, () => console.log(`Server running at ${port}`));
}

module.exports = app;
