// object  - a data type that is used to represeng real world object
/*
    syntax:
        let objectName = {
            propertyA: valueA,
            propertyB: valueB,
        }
*/


let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log('Result from crearting objects' );
console.log(cellphone);
console.log(typeof cellphone);

// creating objects using a constructor function
/*

    Syntax: 
        function objectName(propertyA, PropertyB){
            this.propertyA = propertyA,
            this.propertyB = propertyB,
        }

*/

function Laptop(name,manufactureDate){
    this.name = name,
    this.manufactureDate = manufactureDate
}

// create new instances of the laptop object 
let laptop = new Laptop ('Lenovo', 2008);
console.log('Result from creating objects using objects constructors:');
console.log(laptop)

let myLaptop = new Laptop ('MacBook Air', 2020);
console.log('Result from creating objects using objects constructors:');
console.log(myLaptop)

// accessing object properties
// Using the dot notation good use ung dot notation instead of square bracket

console.log('result from dot notation ' + myLaptop.name);

// using square bracket notation
console.log('result from square bracket notation: ' + myLaptop['name']);

// Access array objects
// accessing using square braket notation
let array = [laptop, myLaptop];
console.log(array[0]['name']);
console.log(array[0].name);

// initializing adding/deleting/reassiginging object properties

let car = {}

// initializing/adding object properties usig dot notation

car.name = 'honda civic';
console.log('Result from adding properties using dot notation');
console.log(car)

// initializing/adding object properties usig square bracket

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log('Result from adding properties using square bracket notation')
console.log(car)



// deleting object Properties
delete car['manufacture date']
console.log('Result from deleting properties');
console.log(car);


// reassining values to object properties
car.name = "Dodge Charger R/T"
console.log('Result from reassiging properties')
console.log(car);

//Object methods
let person = {
    name: 'john',
    talk: function(){
        console.log('Hi, I am ' + this.name);
    }
}

console.log(person);
console.log('result from object methods: ');
person.talk();

// adding methods to objects
person.walk = function(){
    console.log(this.name+'walked 25 steps forward');
}
person.walk();

// real worl application of objects
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
        let _targetPokemonHealth_ = target.health -= this.attack
		console.log(this.name + ' tackled '+ target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_ " + _targetPokemonHealth_ );
	}
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
}

// create new instance of the pokemons object

let pikachu = new Pokemon(" Pikachu ", 16);
let rattata = new Pokemon(" Rattata ", 8);

pikachu.tackle(rattata);


rattata.tackle(pikachu)


