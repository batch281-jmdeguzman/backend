// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added

const trainer =  {
  name : "Ash Ketchum",
  age: 10,
  pokemon : ["Alakazam", "Altaria", "Rockruff", "Bulbasaur"],
  friends: {
      hoenn: ["May","Max"],
      kanto: ["Brock", "Misty"],
  },
  talk(){
      console.log(this.pokemon[2]+ '!' + "I choose you!" )
  }
}
console.log(trainer)
// Access object properties using dot notation
console.log("Result of dot notation:")
console.log(trainer.name)
// Access object properties using square bracket notation
console.log("Result of square bracket notation:")
console.log(trainer["pokemon"])
// Access the trainer "talk" method
console.log("Result of talk method")
trainer.talk()
// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){
  // Properties
  this.name = name;
  this.level = level;
  this.health = 2 * level
  this.attack = level

  // Methods
  this.tackle = function(target){
     let _targetPokemonHealth_ = target.health -= this.attack
      console.log(this.name + ' tackled ' + target.name)
      console.log(target.name + " health is now reduced to "+ _targetPokemonHealth_)
      if (_targetPokemonHealth_ <= 0){
          this.faint(target);
      }
  },
      this.faint = function(target) {
      console.log(target.name + " fainted");
  }
}

// Create/instantiate a new pokemon
let alakazam = new Pokemon("Alakazam", 10);
console.log(alakazam)
// Create/instantiate a new pokemon
let altaria = new Pokemon("Altaria", 60);
console.log(altaria)
// Create/instantiate a new pokemon
let rockruff = new Pokemon("Rockruff", 200);
console.log(rockruff)
// Invoke the tackle method and target a different object
alakazam.tackle(rockruff)
console.log(rockruff)
// Invoke the tackle method and target a different object
altaria.tackle(rockruff)
console.log(rockruff)






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
  module.exports = {

      trainer: typeof trainer !== 'undefined' ? trainer : null,
      Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

  }
} catch(err){

}
