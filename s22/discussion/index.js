// console.log("hello World");

// array methods
// mutator methods
let fruits =["Apple","Orange","Kiwi","Dragon Fruit"]
console.log("Current Array");
console.log(fruits);


// Push()
// adds an element in the end of an array AND returns the array's lenght
// Syntax: array name.push(elementA)

fruits.push("Mango");
console.log("Mutated array from push method");
console.log(fruits); 

let fruitslenght = fruits.push("Mango");
console.log(fruits.length)
console.log("Mutated array from push method");
console.log(fruits); 


// adding multiple elements to an array
fruits.push("Avocado","Guava");
console.log("Mutated Array from push Method");
console.log(fruits)

// pop()

/*
-Removes the last element in an array and return the remove element
-Syntax: arrayName.pop()
*/

let removedFruit = fruits.pop()
console.log(removedFruit);
console.log("Mutated Array from Pop Method");
console.log(fruits)

//unshift
/*
    -adds one or more elements at the begining og an array
    // Syntax: array name.unshift('elementB')
*/

fruits.unshift ('lime','Banana');
console.log("Mutated Array from Unshift Method");
console.log(fruits)


//shift
/*
    -removes one or more elements at the begining og an array AND returns the remove element
    // Syntax: array name.shift('elementB','elementA')
*/

let anotherFruit = fruits.shift ();
console.log(anotherFruit)
console.log("Mutated Array from Unshift Method");
console.log(fruits)

//splice()

/*
    -simultaneously removes element from a specified index and adds elements
    Syntax: arrayName.splice(startingIndex,deleteIndex)
    
*/

// fruits.splice(2,3,"Calamansi","Lime","Cherry")
// console.log("Mutated Array from Splice Method");
// console.log(fruits)


fruits.splice(2,3,"Calamansi","Lime")
console.log("Mutated Array from Splice Method");
console.log(fruits)


// sort()

/*
    -rearrange the array element in alpha numeirc order
    // Syntax: arrayName.sort()
*/

fruits.sort()
console.log("Mutated Array from sort Method");
console.log(fruits)

// reverse() sort

/*
    -reverses the order of array elements 
      syntax: arrayName.reverse()  
    
*/

fruits.reverse()
console.log("Mutated Array from reverse Method");
console.log(fruits)

// Non-Mutator Methods
/*
    - METHODS are function that do not modify or change an array after they're created

    These method do not manipulate the original array performing various task such as returning form array and combining array

*/
let countries = ["US","PH","CAN","SG","PH","FR","GE","CH","KR"]

// indexOf('element)

/*
    return the index number of the first matcing element  found in an array.
    If no match was found, the result will be -1
    The search process will be done from the first element proceding to the last element.

    syntax: 
    arrayName.indexOf(searchValue)
    arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf('PH')
console.log("Result of indexOf: "+firstIndex)

let invalidCountry = countries.indexOf('BR')
console.log("Result of indexOf: "+invalidCountry)

// lastIndexOf()

/*

    -returns the index number of the last matching elemen founf in an array
    - the seach process will be done from last elemetn proceing to first element

*/

let lastIndex = countries.lastIndexOf('PH')
console.log("Result of indexOf: "+lastIndex)

let lastIndexStart = countries.lastIndexOf('PH',6)
console.log("Result of indexOf: "+lastIndexStart)


//slice()
/*
    -Portion/slice element from an array and return a new array 

    Syntax:

    arrayName.slice(startingIndex)
    arrayName.slice(startingIndex, endingIndex)


 */                   

                //       0     1    2    3      4    5   6   7   8
    // let countries = ["US","PH","CAN","SG","PH","FR","GE","CH","KR"]


    let slicedArrayA = countries.slice(2);
    console.log('Result from slice method')
    console.log(slicedArrayA)
                                        //-1
    let slicedArrayB = countries.slice(2,4);
    console.log('Result from slice method')
    console.log(slicedArrayB)

    let slicedArrayC = countries.slice(-3);
    console.log('Result from slice method')
    console.log(slicedArrayC)


    // tostring()
    // Returns an array as string separated by commas
    let stringArray = countries.toString()
    console.log(stringArray)
    console.log(stringArray[0])

    // concat
    //combine 2 array and returns the combie result

    let tasksArrayA = ['drink html', 'eat javascript'];
    let tasksArrayB = ['inhale css', 'breathe sass'];
    let tasksArrayC = ['get git', 'be node'];

    let task = tasksArrayA.concat(tasksArrayB, tasksArrayC);
    console.log('Result from concat Method')
    console.log(task)


    let task2 = tasksArrayA.concat(tasksArrayB, tasksArrayC);
    console.log('Result from concat Method')
    console.log(task2)

     let task3 = tasksArrayA.concat("tasksArrayB", "tasksArrayC");
    console.log('Result from concat Method')
    console.log(task3)


    //join method join()   
    //returns an array as string separated by specified separator strings

    let users =["John",'Juan','Dudong']
    console.log(users.join('-'))

    /*
        - Iteration methods are loops designed to perform repetitive tasks on arrays
        - Iteration methods loops over all items in an array.
        - Useful for manipulating array data resulting in complex tasks
        - Array iteration methods normally work with a function supplied as an argument
        - How these function works is by performing tasks that are pre-defined within an array's method.
    */

        //foreach() 
/*
// forEach()
    /*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
        - It's common practice to use the singular form of the array content for parameter names used in array loops
        - forEach() does not return anything.
        - Syntax
            arrayName.forEach(function(indivElement) {
                statement
            })
    */


task2.forEach(function(task){
console.log(task)

})

//using foreach() for filtering all element
filteredTasks = []
task2.forEach(function(task){
    if(task.length > 10){
        filteredTasks.push(task)
    }

})
console.log(filteredTasks)

//map()
//map() is a method that iterates over an array and returns a new array
/* 
        - Iterates on each element AND returns new array with different values depending on the result of the function's operation
        - This is useful for performing tasks where mutating/changing the elements are required
        - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
        - Syntax
            let/const resultArray = arrayName.map(function(indivElement))
    */
let numbers = [1,2,3,4,5]
let numberMap = numbers.map(function(number){
    return number * number
})

console.log("Original Array: ")
console.log(numbers)

console.log("Result Of map method: ")
console.log(numberMap)


//map() vs forEach()

let numberForEach = numbers.forEach(function(number){

    return number * number

})

console.log(numberForEach);//undefined. 

//forEach(), loops over all items in the array as does map(), but forEach() does not return a new array.

//every
//check if all statement in an array meet the given condition

// let numbers = [1,2,3,4,5]
let allValid = numbers.every(function(number){
    return number < 2
})
console.log(allValid)

//some
//check if some stattement in an array meet the given condition

let someValid = numbers.some(function(number){
    return number<2
})
console.log(allValid)

// filter()
//return new array that contains element which meets the given condition


let filterValid = numbers.filter(function(number){
    return(number> 2)
})
console.log(filterValid)

//include()
// include method checks if the argument passed can be foud in the array
let products = ['Mouse', 'Keyboard', 'Laptop','Monitor']
let productFound1 = products.includes("Mouse")
console.log(productFound1)
let productFound2 = products.includes("Headset")
console.log(productFound2)

/*
        - Methods can be "chained" using them one after another
        - The result of the first method is used on the second method until all "chained" methods have been resolved
        - How chaining resolves in our example:
            1. The "product" element will be converted into all lowercase letters
            2. The resulting lowercased string is used in the "includes" method
    */
            let filteredProducts = products.filter(function(product){
                return product.toLowerCase().includes('a')
            })
            console.log(filteredProducts)


            // reduce()
    /*
    
    Evaluates elements from left to right and returns the array into a single value
    syntax:
    let/const resultArray = arrayName.reduce(accumulator, currentValue){
        return exression/operation
    })
    
    - The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
        - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elemen
----
            - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
            - The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
- How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
    */

    let iteration = 0
    let reduceArray = numbers.reduce(function(acc,cur){
        console.warn('Current Iteration: ' + ++iteration);
        console.log('accumulator' +acc)
        console.log('current value: ' +cur)

        return acc+cur
    })

    console.log(reduceArray)

    // reducing string arrays
    let list = ['Hello', 'Again', 'World'];

    let reducedJoin = list.reduce(function(x, y) {
        return x + ' ' + y;
    });
    console.log("Result of reduce method: " + reducedJoin);
