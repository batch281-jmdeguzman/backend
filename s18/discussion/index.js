// /function with parameters

function printName(name){
	console.log("My Name is " + name);
}

printName("Juana");
printName("John");


let sampleVariable = "bella";
printName(sampleVariable);


function checkDivisibilityBy8 (num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " Divided by 8 is: " + remainder);
	let isDivisibleby8 = remainder === 0;
	console.log("is " + num + " Divisible by 8?");
	console.log(isDivisibleby8);

}

checkDivisibilityBy8(28);

// function as agruments

 function agrumentFunction (){
 	console.log("This Function was passed as an agrument before the message was printed");

 }

 function invokeFunction(agrumentFunction){
 	agrumentFunction();
 }

 invokeFunction(agrumentFunction);


// using multiple parameters
 function createFullName(firstName, middleName, lastName){
 	console.log(middleName+' '+lastName +' '+firstName);

 }

 createFullName('juan','Perez','dela cruz');

 // using variable as arguments

 let firstName="john";
 let middleName="doe";
 let lastName="smith";
 createFullName(firstName, lastName, middleName)


 // return statement  - allows us to output a value from a function to be passed to the line/block of code that invoke the function

 function returnFullName(firstName, middleName, lastName){
 	return firstName + ' ' + middleName + ' ' + lastName;
 	console.log("This message will not be printed.");
 }
 let completeName = returnFullName("Jeffrey", "Smith", "besos");
 console.log(completeName);

 function returnAddress(city, country){
 	let fullAddress = city + ', '+ country;
 	return fullAddress;
 }
 let myAddress = returnAddress("Cebu City", "Philippines");
 console.log(myAddress);



 

 function checkDivisibilityBy8 (num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " Divided by 8 is: " + remainder);
	let isDivisibleby8 = remainder === 0;
	console.log("is " + num + " Divisible by 8?");
	console.log(isDivisibleby8);

}

checkDivisibilityBy8(28);